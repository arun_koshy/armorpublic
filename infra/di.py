import boto3
import pprint

ec2 = boto3.client("ec2")
response = ec2.describe_instances()
pp = pprint.PrettyPrinter(indent=1, width=41, compact=True)
pp.pprint(response)
