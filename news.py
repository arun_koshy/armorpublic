import feedparser
from flask import Flask, render_template

app = Flask(__name__)

RSS_FEEDS = {'bbc': 'http://feeds.bbci.co.uk/news/rss.xml',
             'cnn': 'http://rss.cnn.com/rss/edition.rss',
             'fox': 'http://feeds.foxnews.com/foxnews/latest',
             'iol': 'http://www.iol.co.za/cmlink/1.640'}


@app.route("/")
@app.route("/<publication>")
def get_news(publication="bbc"):
    try:
        feed = feedparser.parse(RSS_FEEDS[publication])
    except KeyError:
        return """<html><body>{0} is not available in our \
        feeds.</body></html>""".format(publication.upper())
    except BaseException:
        return """<html><body>Something went wrong.</body></html>"""
    return render_template("home.html", articles=feed['entries'])


if __name__ == "__main__":
    app.run(port=5000, debug=True)
